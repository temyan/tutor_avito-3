package lesson11;

public class Main {
    public static void main(String[] args) {
        MySQL mysql = new MySQL("binocla", "123123");
        MongoDB mongoDB = new MongoDB("binocla", "123123");
        // Придется для каждого конкретного случая создавать НОВЫЙ экземпляр
        // С помощью абстрактного класса можно сделать следующую штуку:

        Database database = new MySQL("qweqew", "qweqwe");
        database.qwe();
        database = new MongoDB("asd", "zxc");
        // Занимает меньше памяти (используем только 1 Database)
        // Функционал класса слева будет таким, как у класса справа
        // Произошло восходящее преобразование: то есть, класс слева > класса справа
        // мы можем использовать только методы, которые есть у класса слева


    }
    // Абстракция -> это подход к обособлению реализаций от какой-либо жесткой связи (сильная связность)
    // В Java абстракция достигается с помощью двух способов: абстрактные классы ИЛИ/И интерфейсы

    // Класс, который работает с платежной системой / базой данных
    // Вы написали методы, которые сохраняют в эту БД / подтверждают платеж в системе платежей
    // Удаляют из БД -> MongoDB
    // Обновляют в БД
    // "Достают" информацию в БД
    // MySQL -> MongoDB -> PostgreSQL
}

// Java < 8
// Абстрактный класс -> это класс в Java, который обязательно содержит хотя бы один абстрактный метод
// Абстрактный метод -> это метод, который не содержит код (не содержит реализации)
// ключевое слово abstract применимо к классам и методам

// Java >= 8
// Абстрактный класс -> это класс в Java, который может содержать хотя бы один абстрактный метод
// Абстрактный метод -> это метод, который не содержит код (не содержит реализации)
// ключевое слово abstract применимо к классам и методам

abstract class Database {
    private String login;
    private String password;

    public void qwe() {
        System.out.println("qwe");
    }

    public Database(String login, String password) {
        this.login = login;
        this.password = password;
        connectToDatabase();
    }

    public abstract void connectToDatabase(); // Мы завели метод с названием connectToDatabase, который ничего не принимает и ничего не возвращает
    public abstract void add();
    public abstract void delete();
    public abstract void update();
    public abstract void get();
    // абстрактные методы НЕ содержат код (!!!)
    // При наследовании от абстрактного класса -> обычные классы будут вынуждены РЕАЛИЗОВАТЬ все АБСТРАКТНЫЕ методы;
    // из-за того, что джава НЕ умеет компилировать абстрактный код (код БЕЗ реализации) мы не можем с вами объявить экземпляр класса Human
}

class MongoDB extends Database {
    public MongoDB(String login, String password) {
        super(login, password);
    }

    @Override
    public void connectToDatabase() {
        System.out.println("Connected to MongoDB");
    }

    @Override
    public void add() {
        System.out.println("ADD * to TABLE ...");
    }

    @Override
    public void delete() {

    }

    @Override
    public void update() {

    }

    @Override
    public void get() {

    }
}

class MySQL extends Database {

    public MySQL(String login, String password) {
        super(login, password);
    }

    @Override
    public void connectToDatabase() {
        System.out.println("Connected to MySQL");
    }

    @Override
    public void add() {
        System.out.println("SET into table value ?");
    }

    @Override
    public void delete() {

    }

    @Override
    public void update() {

    }

    @Override
    public void get() {

    }
}




