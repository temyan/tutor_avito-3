import java.util.Arrays;

public class Q1 {
    public static void main(String[] args) {
        variant1();
        System.out.println();
        variant2();
        System.out.println();
        variant3();
    }


    public static void variant1() {
        int[] ar = {1, 2, 3};
        // int[] ar0 = ar; // присваиваем ссылку, а не лежащие внутри значения (то есть ar <=> ar0)
        int[] arNew = Arrays.copyOf(ar, ar.length);
        arNew[0] = 100;
//        System.out.println(Arrays.toString(ar));
        // sout(ar) -> {1, 2, 3}
        // sout(ar0) -> {100, 2, 3}
        for (int i = 0; i < ar.length; i++) {
            System.out.print(ar[i] + " "); // 1 2 3
        }
        System.out.println();
        for (int i = 0; i < arNew.length; i++) {
            System.out.print(arNew[i] + " ");
        }
    }

    public static void variant2() {
        int[] ar1 = {1, 2, 3};
        int[] ar2 = ar1.clone();
        ar2[0] = 100;
        for (int i = 0; i < ar1.length; i++) {
            System.out.print(ar1[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < ar2.length; i++) {
            System.out.print(ar2[i] + " ");
        }

    }

    public static void variant3() {
        int[] ar1 = {1, 2, 3};
        int[] ar2 = new int[ar1.length]; // пустой
        for (int i = 0; i < ar2.length; i++) {
            ar2[i] = ar1[i];
        }
        ar2[0] = 100;
        for (int i = 0; i < ar1.length; i++) {
            System.out.print(ar1[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < ar2.length; i++) {
            System.out.print(ar2[i] + " ");
        }
    }
}
