package lesson7;

public class Main {
    public String name;
    public static int age;
    // static -> модификатор статичности, которое показывает, что данное поле или метод являются общими
    // Более того, помеченный staticом код преобразуется в байт-код на этапе компиляции (Ранее связывание)

    public static void main(String[] args) {

        // + в том, что мы можем напрямую (через название класса и точку) обращаться к методам/полям
        // - в том, что изменив static поле (age) мы изменили age у всех потенциальных объектов Person
        // Методы, помеченные static не могут переопределяться
        // Раннее связывание (статичное связывание) -> Код при запуске (на этапе компиляции) сразу собирается в байт-код, потому что он уникальный/глобальный. То есть, вариативности этого кода нет. А значит мы сразу можем его записать и оптимизировать в байт-коде
        // Позднее связывание (нестатичное связывание) -> Код при запуске (на этапе компиляции) НЕ становится байт-кодом. Потому что Java заранее не в курсе (не знает) какую реализацию метода/какое поле вы используете в программе (Например, есть три класса с одинаковым названием метода hello, но с разной реализацией (пометка: эти три класса родственны) и джава не знает, что из этого заранее выбирать, то есть она не следит за использованием конкретного метода)
        // в static методе все созданные переменные static
        // в static методах можно использовать только static переменные
        // в non-static методах можно использовать и static, и non-static.
        Person.configureDB();
        Person p1 = new Person();
        Person p2 = new Person();
        String[] s = {"1", "0", "0"}; // 1 0 0
        String[] s1 = {"9", "9", "5"}; // 9 9
        // [0...9]
        p1.name = "Blabla";
        p2.name = "QWeqwe";
        Person.age = 2;
        p1.show();
        p2.show();
        Child c = new Child();
        c.show();
    }

//    public static void show() {
//        System.out.println("name " + name + " age " + age);
//    }
}

class Person {
    public String name; // индивидуальное (для каждого)
    public static int age; // global (Меняет у всех)
    public static String dbURL;

    // public static String urlToDatabase = "..."
    public static void configureDB() {
        // ...
        dbURL = "jdbc:postgres://localhost:5432/postgres";
        // ...
        System.out.println("Successfully");
    }

    public void show() {
        System.out.println(name + " " + age);
    }
}

class Child extends Person {
    @Override
    public void show() {
        System.out.println("Yea");
    }
}
