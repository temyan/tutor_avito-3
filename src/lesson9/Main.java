package lesson9;

public class Main {


    public static void main(String[] args) {
//        Person1 person1 = new Person1();
//        person1.name = "Sergey";
//        person1.age = -20;
//        System.out.println(person1.name);
//        System.out.println(person1.age);
//        Person p = new Person("Sergey", -20);
//        System.out.println(p.name);
//        System.out.println(p.age);
        Student st = new Student("Sergey", 19, 2);
        Student st2 = new Student(2);
        System.out.println(st.name);
        System.out.println(st.age);
        System.out.println(st.course);

        System.out.println();
        System.out.println(st2.name);
        System.out.println(st2.age);
        System.out.println(st2.course);
//        Person p = st2; // Пример обобщения - восходящее преобразование
//        System.out.println(p.name);
//        System.out.println(p.age);
    }
}

class Person {
    String name;
    int age;

    // По умолчанию в любом классе конструктор уже есть (Правило) - пустой
    //

    public Person(String name) {
        this.name = name;
    }


    public Person(String name, int age) {
        if (age < 0 || age > 100) {
            System.out.println("Некорректные значения");
            // return; // Аналог break завершает работу текущего метода
            System.exit(0); // Аналог return или же аналог break -> Завершает работу программы (вообще)
        }
        this.name = name;
        this.age = age;
    } // Пустой конструктор (тот, который по умолчанию) - исчезает

    public Person() {
    } // Пустой конструктор позволит дочерним НЕ использовать super с обязательной передачей каких-то параметров (потому в этом конструкторе их банально нет)
    // Однако стоит учесть, что сразу получая доступ к полям родителя (и его методам) вы рискуете либо пустыми значениями полей (если у дочернего не присвоите)
    // либо некорректным значениям (если у дочернего поставите age = 192131 - сработает). Для фикса - либо все-таки обращаемся к родительскому конструктору, у которого есть такие проверки (как выше)
    // Либо пишем дополнительно свои проверки (на тот случай, если у родительского каких-то нехватает или они не совсем подходят под конкретный дочерний класс) иначе - нет смысла в дублировании кода (например, проверок)
}

class Student extends Person {
    int course;

    public Student(String name, int age, int course) {
        super(name, age);
        // Почему выше super обязателен?
        // Конструктор - входная "дверь" для работы с полями и методами какого-либо класса
        // Чтобы работать с полями name и age мы обязаны войти через "дверь"
        // В качестве "двери" у нас выступает конструктор родительского класса public Person(String name, int age)
        // Других конструкторов у нас не имеется (то есть дверь только одна)
        //
        this.course = course;
    }

    public Student(int course) {
        this.name = "Default";
        this.age = 1123123;
        // super("Default", age);
        // super(); // По умолчанию есть - можно тут это не писать
        this.course = course;
    }
}

class Person1 {
    String name; // у всех ссылочных типов данных null
    int age; // 0
}
