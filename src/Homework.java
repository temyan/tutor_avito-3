import java.util.Arrays;
import java.util.Scanner;

public class Homework { //Home work - "Strange words"
    /*
    Дан набор из n слов, состоящих из маленьких латинских букв.
    Будем называть слово странным, если в нем встречаются 3 или более гласные буквы подряд.
    Ваша задача — удалить из данного набора все странные слова.
    Гласными буквами в латинском алфавите считаются e,y,u,i,o,a.
    Входные данные
    В первой строке содержится число n — количество слов в наборе, n не превосходит 100.
    Далее в n строках по одному на строке содержатся слова из набора. Слова состоят только из маленьких латинских букв.
    Длина каждого слова не менее 1 и не более 20 символов.

    Выходные данные
    Выведите все слова из набора, не являющиеся странными.
    Каждое слово выводите на отдельной строке в том порядке, в котором они заданы во входных данных.

     */
    public static void main(String[] args) {
        //создаем одномерный массив для всех подходящих слов ( что бы вывести их всех разом )
        String[] list = {};
        int count = 0;
        //Задаем число строк в программе - rows
        Scanner in = new Scanner(System.in);
        System.out.println("Write any number in a range of 1 to 100");
        int rows = in.nextInt();
        //Проверяем диапазон числа заданной при вводе - не больше 100
        if (rows > 0 && rows < 100) {
            for (int i = 0; i < rows; i++) {
                //Пишем по одному слову на каждой строке (rows) на каждом этапе "for" и проверяем его
                System.out.println("Write any word here. English letters in small register only");
                String word = in.next();
                //Делаем слово маленькими буквами ( на всякий )
                word = word.toLowerCase();
                int len = word.length();
                if(len >= 1 && len < 20){//проверяем длину слова
                    for(int y = 0; y < word.length(); y++){//проверяем каждое слово на принадлежность к латинскому алфавиту
                        char x = word.charAt(y);
                        if (!(x >= 'a' && x <= 'z')) {
                            System.out.println("You wrote the word in another language!");
                            break;
                        }
                    }
                    //Ищем в слове гласные по условию ( версия вывода подходящих слов на каждом этапе for)
                    /*String word1 = word.replaceAll("[eyuioa]{3}", "");
                    //проверяем есть ли изменения
                    if(word1.equals(word)){
                        System.out.println(word1);
                    }*/
                    String word1 = word.replaceAll("[eyuioa]{3}", "");
                    //проверяем есть ли изменения - версия вывода подходящих слов с помощью массива в конце программы
                    if(word1.equals(word)){
                        list = Arrays.copyOf(list, list.length+1);
                        list[count] = word1;
                        count++;
                    }
                    //Версия вывода подходящих слов с помощью массива в конце программы
                }
            }
        }
        else {
            System.out.println("The length is out of range!. Try again");
        }
        //выводим все подходящие слова
        /* abacaba aabacaba aaabacaba qwerty yyyyyyyyyeaaaaah noyes */
        for(int j = 0; j < list.length; j++){
            System.out.println(list[j]);
        }



    }
}

class Home2045 {
    public static void main (String[] args){
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        //System.out.println(str);
        //String str = "Hello   world!  How ,and,    yes . Good ! How?";
        // galileo   galilei was an   italian physicist  ,mathematician,  astronomer
        str = str.replaceAll("\\s+"," ");
        System.out.println(str);
        str = str.replaceAll("\\s[,]",",").replaceAll("\\s[.]",".").replaceAll("\\s[!]","!").replaceAll("\\s[?]","?");
        System.out.println(str);
        str = str.replaceAll("[.]\\b+",". ").replaceAll("[,]\\b+",", ").replaceAll("[!]\\b+","! ").replaceAll("[?]\\b+","? ");
        System.out.println(str);
    }
}